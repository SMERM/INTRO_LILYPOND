# INTRO_LILYPOND
Repository del Corso di Introduzione al software notazionale [LILYPOND](http://lilypond.org/index.it.html) tenuto dal M° Citera

Lezioni:

* Sabato 5 ottobre 2019 ore 10-13 > [Riassunto della Prima Lezione](https://github.com/SMERM/INTRO_LILYPOND/tree/master/05102019_Lezione_01)
* Venerdì 11 ottobre 2019 ore 10-13 > [Riassunto della Seconda Lezione](https://github.com/SMERM/INTRO_LILYPOND/tree/master/11102019_Lezione_02)
* Venerdì 18 ottobre 2019 ore 10-13 > [Riassunto della Terza Lezione](https://github.com/SMERM/INTRO_LILYPOND/tree/master/18102019_Lezione_03)
* Giovedì 31 ottobre 2019 ore 10-13 > [Esempi citati nella Quarta Lezione](https://github.com/SMERM/INTRO_LILYPOND/tree/master/31102019_Lezione_04)

* Alcuni Template > [Template, PaperSize e PDF](https://github.com/SMERM/INTRO_LILYPOND/tree/master/Template)



**Come Installare LilyPond:**
* [Scaricare il file di installazione di LilyPond](http://lilypond.org/download.it.html)
* Eseguire l'installer

**Come Installare Frescobaldi:**

Frescobaldi è un editor che utilizza LilyPond per la realizzazione di partiture, (mostra in tempo reale le modifiche realizzate su di un file LilyPond). Frescobaldi è un software libero, disponibile gratuitamente per macOs, Windows e Linux.
* [Scaricare il file di installazione di Frescobaldi](http://www.frescobaldi.org/download)
* Eseguire l'installer
