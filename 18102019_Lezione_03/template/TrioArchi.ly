primorigo = \new Staff \with {
				\clef treble 
				instrumentName = "Violino"  }{\primoStrumento}
secondorigo = \new Staff \with { 
				\clef alto
				instrumentName = "Viola" }{\secondoStrumento}
terzorigo = \new Staff \with { 
				\clef bass
				instrumentName = "Violoncello" }{\terzoStrumento}