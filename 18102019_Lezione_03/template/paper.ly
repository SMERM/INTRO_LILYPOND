\paper {  #(set-paper-size "a4" )%'landscape
 			 top-margin = 1\cm
  			after-title-space = 10\mm
  			bottom-margin = 2\cm
  			indent = 20\mm
 		 line-width = 180\mm
 		 line-width = #(- line-width (* mm  3.000000) (* mm 1))
			} 