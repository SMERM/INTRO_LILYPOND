\header {
 % dedication = "Dedication"
  title = \markup{ \fontsize #4 \bold \smallCaps "COMPOSIZIONE" }
  subtitle = \markup{ \fontsize #4 \bold \smallCaps "Sottotitolo" }
  instrument = \markup{ \fontsize #2  \italic "per Orchestra" }
  composer = \markup{ \fontsize #1 \bold \smallCaps "Nome Compositore" }
  arranger = \markup {\vspace#2  \null } %per distanziare titolo da partitura
    
  tagline = " "
}