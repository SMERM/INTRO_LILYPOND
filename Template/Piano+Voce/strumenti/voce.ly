voceMusic = \relative c' { \key g \major  
\autoBeamOff
\tempo "Andante" 4 = 90
						g'8 a b c  d e fis g 		\break
						g,8 [a] b [c] 
							d[ e fis g ]
											\bar"|."
									
				 }

voceLyrics = \lyricmode { Sol -- la -- si -- do -- re -- mi -- fa -- sol
					 Me -- lis -- ma __  }