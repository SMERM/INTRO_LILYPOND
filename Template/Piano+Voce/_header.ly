\header {
 % dedication = "Dedication"
  title = \markup{ \fontsize #2 \bold \smallCaps "LIEDER" }
  subtitle = \markup{ \fontsize #1 \bold \smallCaps "Sottotitolo" }
  instrument = \markup{ \fontsize #1  \italic "Piano e Voce" }
  composer = \markup{ \fontsize #1 \bold \smallCaps "Nome Compositore" }
  arranger = \markup {\vspace#2  \null } %per distanziare titolo da partitura
    
  tagline = " "
}