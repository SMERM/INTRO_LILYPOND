\header {
 % dedication = "Dedication"
  title = \markup{ \fontsize #2 \bold \smallCaps "COMPOSIZIONE" }
  subtitle = \markup{ \fontsize #1 \bold \smallCaps "per Quartetto" }
  instrument = \markup{   \italic " (Vn. V.la Cello Pf.)" }
  composer = \markup{ \fontsize #1 \bold \smallCaps "Nome Compositore" }
  arranger = \markup {\vspace#2  \null } %per distanziare titolo da partitura
    
  tagline = " "
}