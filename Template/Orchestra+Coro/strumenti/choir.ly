sopranoMusic = \relative c' { \key g \major  
						\repeat unfold 2 {g' a b c d e fis g} 
				 }

sopranoLyrics = \lyricmode { Ah -- _ ah -- _ ah }









altoMusic = \relative c' { \key g \major 

			\repeat unfold 2 {g' a b c d e fis g} 
	  }

altoLyrics = \lyricmode { Eh -- _ eh -- _ eh }






tenorMusic = \relative c' { \clef "treble_8" \key g \major 

				\repeat unfold 2 {g a b c d e fis g} 
	  }

tenorLyrics = \lyricmode { Oh -- _ oh -- _ oh }






bassMusic = \relative c { \clef bass \key g \major 

			\repeat unfold 2 {g a b c d e fis g} 
 }


bassLyrics =  \lyricmode { Uh -- _ uh -- _  uh }