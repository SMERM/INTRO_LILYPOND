#(set-global-staff-size 20)

\paper {
  #(set-paper-size "a3")
  ragged-last-bottom = ##f
  top-margin = 15
  line-width = 267
  left-margin = 20
  bottom-margin = 15
 %after-title-space = 50\mm
   %first-page-number = #1 
 
 print-page-number = ##t
  print-first-page-number = ##t
  
  #(include-special-characters)
  

}



