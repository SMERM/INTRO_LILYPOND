\header {
 % dedication = "Dedication"
  title = \markup{ \fontsize #2 \bold \smallCaps "COMPOSIZIONE" }
  subtitle = \markup{ \fontsize #1 \bold \smallCaps "Sottotitolo" }
  instrument = \markup{ \fontsize #1  \italic "per Quartetto d'Archi " }
  composer = \markup{ \fontsize #1 \bold \smallCaps "Nome Compositore" }
  arranger = \markup {\vspace#2  \null } %per distanziare titolo da partitura
    
  tagline = " "
}